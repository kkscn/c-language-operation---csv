# C语言操作CSV

#### 介绍
使用C语言实现对CSV文件的操作

#### 使用说明

1.  使用C语言实现了读取CSV格式的文件，并将其用链表存储；
2.  使用C语言实现了对字符串的划分（使用`Split()`函数）。

#### 定义数据类型
```C
//Node用于存储具体数据
typedef struct Node{
    char *Data;
    struct Node *Next;
}Node;
//Line是每一行的头，FirstNode指向带有头结点的Node链表
typedef struct Line{
    Node *FirstNode;
    struct Line *Next;
}Line;
//Head是整个文件的头，FirstLine指向带有头结点的Line链表
typedef struct Head{
    Line *FirstLine;
    int RowNum, ColNum; //RowNum代表行数，ColNum代表列数
} Head;
```

#### 实现函数
```C
/*Split实现对字符串str的划分，以其中包含的字符c为界限
 *FirstNode为指向一带有头结点的Node链表
 *返回值为划分的节点个数
 */
int Split(char str[], Node *FirstNode, char c);
/*Read实现对CSV文件的读取
 *返回值为指向Head类型的指针
 */
Head *Read(char filePath[]);
```