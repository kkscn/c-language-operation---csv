#include<stdio.h>
#include<stdlib.h>
#include<string.h>
//Node用于存储具体数据
typedef struct Node{
    char *Data;
    struct Node *Next;
}Node;
//Line是每一行的头，FirstNode指向带有头结点的Node链表
typedef struct Line{
    Node *FirstNode;
    struct Line *Next;
}Line;
//Head是整个文件的头，FirstLine指向带有头结点的Line链表
typedef struct Head{
    Line *FirstLine;
    int RowNum, ColNum; //RowNum代表行数，ColNum代表列数
} Head;

int Split(char str[], Node *FirstNode, char c);
Head *Read(char filePath[]);

Head *Read(char filePath[])
{
    Head *fileHead = (Head*)malloc(sizeof(Head));
    fileHead->FirstLine = (Line*)malloc(sizeof(Line));  //申请一个节点作为头结点，不保存数据，最后删除
    fileHead->ColNum = 0;
    fileHead->RowNum = 0;
    Line *Lp = fileHead->FirstLine;
    Lp->FirstNode = NULL;
    Lp->Next = NULL;
    FILE *fp = fopen(filePath, "r");
    char temp[1000];
    char *p = fgets(temp, 1000, fp);
    while(p)
    {
        Lp->Next = (Line*)malloc(sizeof(Line));
        Lp = Lp->Next;
        Lp->Next = NULL;
        Lp->FirstNode = (Node *)malloc(sizeof(Node));
        fileHead->ColNum = Split(p, Lp->FirstNode, ',');
        fileHead->RowNum += 1;
        p = fgets(temp, 1000, fp);
    }
    fclose(fp);
    return fileHead;
}

int split(char str[], Node *FirstNode, char c)
{
    Node *Np = FirstNode;
    int i, j;
    int cnt = 0;
    char temp[1000];
    j = 0;
    for (i = 0; str[i]; i++)
    {
        if(str[i]==c)
        {
            cnt++;
            temp[j] = 0;
            j = 0;
            Np->Next = (Node *)malloc(sizeof(Node));
            Np = Np->Next;
            Np->Next = NULL;
            Np->Data = (char *)malloc(sizeof(char) * (strlen(temp) + 1));
            strcpy(Np->Data, temp);
        }
        else
        {
            temp[j] = str[i];
            j++;
        }
    }
    temp[j] = 0;
    Np->Next = (Node *)malloc(sizeof(Node));
    Np = Np->Next;
    Np->Next = NULL;
    Np->Data = (char *)malloc(sizeof(char) * (strlen(temp) + 1));
    strcpy(Np->Data, temp);
    cnt++;
    return cnt;
}